#ifndef CALC_AST_TREE_H
#define CALC_AST_TREE_H

#include <stdbool.h>
#include "stdlib.h"
#include "list.h"

typedef enum request_type {
    UNDEFINED,
    OPEN_FILE_REQ,
    CREATE_FILE_REQ,
    CLOSE_FILE_REQ,
    CREATE_SCHEMA_REQ,
    DELETE_SCHEMA_REQ,
    ADD_VERTEX_REQ,
    SELECT_REQ
} request_type;

typedef enum data_type {
    INTEGER_AT,
    BOOL_AT,
    FLOAT_AT,
    STRING_AT,
    REF_AT
} data_type;

typedef enum compare_option {
    EQUALS_CMP,
    GREATER_CMP,
    GREATER_EQUALS_CMP,
    LESS_CMP,
    LESS_EQUALS_CMP,
    NOT_EQUALS_CMP,
    CONTAINS_CMP
} compare_option;

static char *const select_condition_as_strings[] = {
        [EQUALS_CMP] = "=",
        [GREATER_CMP] = ">",
        [GREATER_EQUALS_CMP] = ">=",
        [LESS_CMP] = "<",
        [LESS_EQUALS_CMP] = "<=",
        [NOT_EQUALS_CMP] = "!=",
        [CONTAINS_CMP] = "CONTAINS SUBSTRING"
};

typedef enum statement_type {
    SELECT_CONDITION_TYPE,
    OUT_TYPE,
    DELETE_TYPE
} statement_type;

typedef struct file_struct {
    char *filename;
} file_struct;

typedef struct attribute {
    char *attr_name;
    data_type attr_type;
    char *schema_ref_name;
} attribute;

typedef struct create_schema_struct {
    char *name;
    arraylist *attributes;
} create_schema_struct;

typedef struct delete_schema_struct {
    char *name;
} delete_schema_struct;

union value {
    int integer_value;
    bool bool_value;
    char *string_value;
    float float_value;
};

typedef struct attribute_value {
    char *attr_name;
    data_type attr_type;
    union value value;
} attribute_value;

typedef struct add_vertex_struct {
    char *name;
    arraylist *attribute_values;
} add_vertex_struct;

typedef struct select_condition {
    char *attr_name;
    compare_option option;
    data_type attr_type;
    union value value;
} select_condition;

typedef struct statement {
    statement_type type;
    union {
        arraylist *conditions;
        char *attr_name;
    };
} statement;

typedef struct ast_tree {
    request_type type;
    char *schema_name;
    union {
        file_struct file;
        create_schema_struct create_schema;
        delete_schema_struct delete_schema;
        add_vertex_struct add_vertex;
        arraylist *statements;
    };
} ast_tree;

inline unsigned int get_array_size(arraylist *l);

void print_request_tree(ast_tree tree);

ast_tree get_request_tree();

#endif
