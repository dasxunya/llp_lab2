struct arraylist {
    unsigned int size; // Count of items currently in list
    unsigned int capacity; // Allocated memory size, in items
    void **body; // Pointer to allocated memory for items (of size capacity * sizeof(void*))
};

typedef struct arraylist arraylist;

void destroy_array(arraylist *l);

void *get_from_array(arraylist *l, unsigned int index);

void add_to_array(arraylist *l, void *item);

void allocate_array(arraylist *l, unsigned int size);

arraylist *create_array();