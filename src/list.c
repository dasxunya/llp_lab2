/**
 * Arraylist implementation
 * (c) 2011 @marekweb
 *
 * Uses dynamic extensible arrays.
 */

#include <stdlib.h>
#include <assert.h>
#include "list.h"

#define INITIAL_CAPACITY 4

arraylist* create_array() {
    arraylist* new_list = malloc(sizeof(arraylist));
    assert(new_list);
    new_list->size = 0;
    new_list->capacity = INITIAL_CAPACITY;
    new_list->body = malloc(sizeof(void*) * new_list->capacity);
    assert(new_list->body);
    return new_list;
}

void allocate_array(arraylist* l, unsigned int size) {
    assert(size > 0);
    if (size > l->capacity) {
        unsigned int new_capacity = l->capacity;
        while (new_capacity < size) {
            new_capacity *= 2;
        }
        void** new_body = realloc(l->body, sizeof(void*) * new_capacity);
        assert(new_body);
        l->body = new_body;
        l->capacity = new_capacity;
    }
}

unsigned int get_array_size(arraylist* l) {
    return l->size;
}

void add_to_array(arraylist* l, void* item) {
    allocate_array(l, l->size + 1);
    l->body[l->size] = item;
    l->size++;
}

void* get_from_array(arraylist* l, unsigned int index) {
    assert(index < l->size);
    return l->body[index];
}

void destroy_array(arraylist* l) {
    free(l->body);
    free(l);
}