//#define YYDEBUG 0

#include "parser.h"
#include "inc/ast_tree.h"
int yyparse();

int main() {
    int yydebug=0;
    yyparse();
    ast_tree tree = get_request_tree();
    print_request_tree(tree);
    return 0;
}

//V('name').has('attr_name', eq(123)).has('ssjjs', contains('meow'));
//createSchema('test', 'f', integer, 's', float, 't', boolean);
//createSchema('test');
//V('name').has('attr1', eq(123), 'attr2', lte(13.1));
//V('test').has('att1', lte(456)).has('att2', contains('how'));
